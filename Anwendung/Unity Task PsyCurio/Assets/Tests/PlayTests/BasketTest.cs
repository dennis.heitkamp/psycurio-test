using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using TMPro;

/**
 * Tests
 * */
public class BasketTest
{
    /*
     * Test if the first item in the basket is put to the right position.
     * */
    [UnityTest]
    public IEnumerator ItemNumberAndPositionTest()
    {
        //arrange
        var cash = new GameObject().AddComponent<BasketHandler>();
        var dummy = new GameObject();
        dummy.AddComponent<ItemInformation>();
        dummy.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy.GetComponent<ItemInformation>().itemName = "Book";
        //var itemCounter = 1;
        cash.SetBasket(new GameObject[5]);
        var slot0 = new GameObject();
        slot0.transform.position = new Vector3(1, 1, 1);
        cash.GetBasket()[0] = slot0;
        cash._init();

        //act
        cash.PutInBasket(dummy);
        yield return new WaitForSeconds(0.5f);

        //assert
        //Assert.AreEqual(itemCounter, cash._GetAmountOfItemsInBasket());
        Assert.AreEqual(dummy.transform.position, slot0.transform.position);
    }

    /*
     * Test if the flag "is basket full" swaps after the fifth element and at most 5 items are in the basket.
     * */
    [UnityTest]
    public IEnumerator MaxItemTest()
    {
        //arrange
        var cash = new GameObject().AddComponent<BasketHandler>();
        var dummy = new GameObject();
        dummy.AddComponent<ItemInformation>();
        dummy.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy.GetComponent<ItemInformation>().itemName = "Book";

        var dummy2 = new GameObject();
        dummy2.AddComponent<ItemInformation>();
        dummy2.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy2.GetComponent<ItemInformation>().itemName = "Book";

        var dummy3 = new GameObject();
        dummy3.AddComponent<ItemInformation>();
        dummy3.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy3.GetComponent<ItemInformation>().itemName = "Book";

        var dummy4 = new GameObject();
        dummy4.AddComponent<ItemInformation>();
        dummy4.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy4.GetComponent<ItemInformation>().itemName = "Book";

        var dummy5 = new GameObject();
        dummy5.AddComponent<ItemInformation>();
        dummy5.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy5.GetComponent<ItemInformation>().itemName = "Book";

        var dummy6 = new GameObject();
        dummy6.AddComponent<ItemInformation>();
        dummy6.GetComponent<ItemInformation>().itemPrice = 10f;
        dummy6.GetComponent<ItemInformation>().itemName = "Book";

        cash.SetBasket(new GameObject[5]);

        var slot0 = new GameObject();
        slot0.transform.position = new Vector3(1, 1, 1);
        cash.GetBasket()[0] = slot0;

        var slot1 = new GameObject();
        slot0.transform.position = new Vector3(2, 1, 1);
        cash.GetBasket()[1] = slot1;

        var slot2 = new GameObject();
        slot0.transform.position = new Vector3(3, 1, 1);
        cash.GetBasket()[2] = slot2;

        var slot3 = new GameObject();
        slot0.transform.position = new Vector3(4, 1, 1);
        cash.GetBasket()[3] = slot3;

        var slot4 = new GameObject();
        slot0.transform.position = new Vector3(5, 1, 1);
        cash.GetBasket()[4] = slot4;

        cash._init();

        //act
        cash.PutInBasket(dummy);
        cash.PutInBasket(dummy2);
        cash.PutInBasket(dummy3);
        cash.PutInBasket(dummy4);

        int amount= cash._GetAmountOfItemsInBasket();

        //assert
        Assert.AreEqual(4, amount);

        //act2
        cash.PutInBasket(dummy5);
        amount = cash._GetAmountOfItemsInBasket();

        //assert2
        Assert.AreEqual(5, amount);

        //act3
        cash.PutInBasket(dummy6);

        //assert3
        amount = cash._GetAmountOfItemsInBasket();
        Assert.AreEqual(5, amount);

        yield return new WaitForSeconds(0.5f);

    }

    /*
     * Calculate the total price.
     * */
    [UnityTest]
    public IEnumerator CalculatePrice()
    {
        //arrange
        var cash = new GameObject().AddComponent<BasketHandler>();
        var dummy = new GameObject();
        dummy.AddComponent<ItemInformation>();
        dummy.GetComponent<ItemInformation>().itemPrice = 17f;
        dummy.GetComponent<ItemInformation>().itemName = "Book";

        var dummy2 = new GameObject();
        dummy2.AddComponent<ItemInformation>();
        dummy2.GetComponent<ItemInformation>().itemPrice = 8f;
        dummy2.GetComponent<ItemInformation>().itemName = "Book";

        cash.SetBasket(new GameObject[5]);

        var slot0 = new GameObject();
        slot0.transform.position = new Vector3(1, 1, 1);
        cash.GetBasket()[0] = slot0;

        var slot1 = new GameObject();
        slot0.transform.position = new Vector3(2, 1, 1);
        cash.GetBasket()[1] = slot1;



        yield return new WaitForSeconds(2f);
        cash._init();

        //act
        cash.PutInBasket(dummy);
        cash.PutInBasket(dummy2);

        cash.Checkout();
        yield return new WaitForSeconds(10f);

        //assert
        Assert.AreEqual(25f, cash._GetTotalPrice());
    }
}
