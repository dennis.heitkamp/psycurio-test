using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;


/**
 * Controll the items the user selected.
 * 
 * */
public class BasketHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject[] basket; // holds info to spawn items on counter
    int maxItemNumber = 5;
    float totalPrice = 0f;
    enum states { NORMAL = 0, ABORT = 1}; //states for skipping the speech bubble
    states state = states.NORMAL; //current state normal speech bubble

    [SerializeField]
    private GameObject speechBubble;
    [SerializeField]
    private GameObject dialogue; // text in the speech bubble
    private Vector3 speechbubbleSize;
    private Vector3 canvasPosition;
    private Tuple<bool, string, float, GameObject>[] items; //save item information, freespace and reference to the object on the counter


 
    /*
     * Init all variables.
     * */
    void Start()
    {
        items = new Tuple<bool, string, float, GameObject>[maxItemNumber];
        // for unit test only 
        if (dialogue == null)
        {
            dialogue = new GameObject();

        }
        // for unit test only 
        if (speechBubble == null)
        {
            speechBubble = new GameObject();

        }


        canvasPosition = dialogue.transform.position;
        speechbubbleSize = speechBubble.transform.localScale;

        for (int i = 0; i < maxItemNumber; i++)
        {
            items[i] = new Tuple<bool, string, float, GameObject>(true, "null", 0f, null);
        }
    
    }

    /*
     * Put a selected item on the counter or reject it.
     * */
    public bool PutInBasket(GameObject item)
    {
        int itemNumber = GetFirstFreeSpace();
        if (itemNumber == -1)
        {
            return false; // item rejected
        }
        Vector3 pos = basket[itemNumber].transform.position; //set position to first free slot on the counter

        if (item.tag == "longObject") // if the object is long rotate it
        {
            Vector3 rot = item.transform.eulerAngles;
            rot.y += 80f;
            item.transform.eulerAngles = rot;
        }

        item.transform.position = pos; // set the position
        string name = item.GetComponent<ItemInformation>().itemName;
        float price = item.GetComponent<ItemInformation>().itemPrice;  
        item.tag = "item";
        item.name = "item_" + itemNumber;
        Tuple<bool, string, float, GameObject> newEntry = new Tuple<bool, string, float, GameObject>(false, name, price, item);
        items[itemNumber] = newEntry;

        return true; // item accepted

    }

    int GetFirstFreeSpace()
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].Item1)
            {
                return i;
            }
        }
        return -1;
    }

    /*
     * Remove an item from the counter. Delete it and edit entry in items.
     * */
    public void RemoveItem(string name)
    {
        //parse
        int idx = name.IndexOf("_") + 1;
        int spotNumber = int.Parse(name.Substring(idx));
        Destroy(items[spotNumber].Item4);
        items[spotNumber] = new Tuple<bool, string, float, GameObject>(true, "null", 0, null);
    }


    /*
     * Start the checkout process.
     * */
    public void Checkout()
    {
        if (state == states.NORMAL) // normal case
        {
            state = states.ABORT;
            speechBubble.SetActive(true);
            StartCoroutine(Bill()); //start listing items
        }
        else // abort listing items
        {
            Reset();
            StopAllCoroutines();
        }
    }

    string FormatTextOnBubble(string n, float p)
    {
        return n + " " + p +"�"; 
    }

    /*
     * Go through items and store the prices and names.
     * */
    string CollectItems()
    {
        string text = "";
        for (int i = 0; i < items.Length; i++)
        {
            string name = items[i].Item2;
            if (name == "null")
            {
                continue;
            }
            float price = items[i].Item3;
            totalPrice += price;

            text += name + ": " + price + "�, ";
        }

        text += "\n in total: " + totalPrice + "�";

        return text;
    }

   

    /*
     * Coroutine to list items.
     * */
    IEnumerator Bill()
    {
        int itemNumber = GetFirstFreeSpace();
        int totalAmount = _GetAmountOfItemsInBasket();

        if ( totalAmount == 0) // if no items were selected
        {
            dialogue.GetComponent<TMP_Text>().text = "You have not selected any items.";
            yield return new WaitForSeconds(2f);
            Reset();
            yield break;
        }
        totalPrice = 0f;

        dialogue.GetComponent<TMP_Text>().text = "You were interested in:";
        yield return new WaitForSeconds(1f);
        string text = CollectItems();
        dialogue.GetComponent<TMP_Text>().text = "";
        yield return new WaitForSeconds(0.5f);   
        dialogue.GetComponent<TMP_Text>().text = text;
        yield return new WaitForSeconds(totalAmount * 2f);
        Reset();
    }

    /*
     * Remove all items from the counter and delete them.
     * */
    public void RemoveAllItems()
    {
        for (int i= 0; i< items.Length; i++)
        {
            Destroy(items[i].Item4);
            items[i] = new Tuple<bool, string, float, GameObject>(true, "null", 0, null);
        }
    }


    public GameObject[] GetBasket()
    {
        return basket;
    }

    public void SetBasket(GameObject[] newBasket)
    {
        basket = newBasket;
    }

    /*
     * Reset all variables.
     * */
    void Reset()
    {
        state = states.NORMAL;
        speechBubble.SetActive(false);
    }

    /*
     * UNIT TEST FUNCTIONS
     * */
    public void _init()
    {
        dialogue = new GameObject();
        dialogue.AddComponent<TextMeshPro>();
        speechBubble = new GameObject();
        items = new Tuple<bool, string, float, GameObject>[5];
        for (int i = 0; i < 5; i++)
        {
            items[i] = new Tuple<bool,string, float, GameObject>(true,"null", 0, null);
        }

    }

    public float _GetTotalPrice()
    {
        return totalPrice;
    }

    public GameObject _GetDialogue()
    {
        return dialogue;
    }

    public int _GetAmountOfItemsInBasket()
    {   
        int amount = 0;
        for (int i = 0; i < items.Length; i++)
        {
            if (!items[i].Item1)
            {
                amount++;
                
            }
        }
        return amount;
    }




}
