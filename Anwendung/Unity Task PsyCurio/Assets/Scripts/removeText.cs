using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Disable help text.
 * */
public class removeText : MonoBehaviour
{
    [SerializeField]
    private GameObject canvas;
    
    public void DisableCanvas()
    {
      canvas.SetActive(false);
    }
    
}
