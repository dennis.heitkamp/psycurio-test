using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class MouseHover : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.GetComponent<Outline>() != null)
            gameObject.GetComponent<Outline>().enabled = false;
    }

    /*
     * Check when the mouse is over the item.
     * */
    void Update()
    {
        bool disable = true;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //shoot raycast
        if (Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.transform != null)
            {
                if (hit.transform.gameObject.name == gameObject.name)
                {
                    //set ouline properties
                    hit.transform.gameObject.GetComponent<Outline>().OutlineColor = Color.yellow;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 5f;
                }
                else
                {
                    disable = false;
                }
            }
            else
            {
                disable = false;
            }
        }
        else
        {
            disable = false;
        }
       //enable or disable outline 
        gameObject.GetComponent<Outline>().enabled = disable;

    }



}
