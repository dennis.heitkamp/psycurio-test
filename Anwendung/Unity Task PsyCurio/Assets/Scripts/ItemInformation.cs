using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Store item information.
 * */
public class ItemInformation : MonoBehaviour
{
    public string itemName;
    public float itemPrice;
 
}
