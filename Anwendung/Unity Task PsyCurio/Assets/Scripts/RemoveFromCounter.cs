using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 
 * Handle the removal of objects on the counter.
 * */
public class RemoveFromCounter : MonoBehaviour
{
    GameObject cashregister;

    //if the object is clicked call Remove()
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //shoot raycast

            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.transform != null)
                {
                    if (hit.transform.gameObject.name == gameObject.name && hit.transform.gameObject.tag == "item") // if this object is hit by the raycast
                    {
                        Remove(); //call other function
                    }
                }
            }

        }
    }

    public void SetCashRegister(GameObject cash)
    {
        cashregister = cash;
    }

    /*
     * Call Remove function in BasketHandler.cs
     * */
    public void Remove()
    {
        cashregister.GetComponent<BasketHandler>().RemoveItem(gameObject.name);
    }

   
}
