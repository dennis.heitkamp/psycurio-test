using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Remove items from counter.
 * */
public class BuyAllItems : MonoBehaviour
{
    [SerializeField]
    private GameObject cashRegister;
  

    /*
     * Call Remove all items in BasketHandler and play a sound.
     * */
   public void BuyItems()
    {
        cashRegister.GetComponent<BasketHandler>().RemoveAllItems();
        GetComponent<AudioSource>().Play();
    }
}
