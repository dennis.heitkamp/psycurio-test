using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Controll the help texts.
 * */
public class showHelpAgain : MonoBehaviour
{
    [SerializeField]
    private GameObject[] gos; 
    
    /*
     * Activate all help texts.
     * */
    public void ActivateAllHelpTexts()
    {
        for (int i= 0; i < gos.Length; i++)
        {
            gos[i].SetActive(true);
        }
    }
}
