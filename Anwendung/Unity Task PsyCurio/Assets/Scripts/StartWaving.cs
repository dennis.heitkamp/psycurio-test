using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Start the waving animation.
 * */
public class StartWaving : MonoBehaviour
{
    Animator animator;

    /*
     * Get all necessary refs.
     * */
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    /*
     * Activate the waving animation.
     * */
    public void StartWave()
    {
        animator.SetBool("IsClicked", true);
    }
}
