using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/**
 * Handle the creation of the clicked objects.
 * */
public class PutInBasket : MonoBehaviour
{
    [SerializeField]
    private GameObject cashregister;


    /*
     * Create a copy of the selected object and keep or destroy it.
     * */
    public void IntoBasket()
    {
        GetComponent<Outline>().enabled = false;
        GameObject copy = Instantiate(gameObject, transform.position, transform.rotation, transform); // create a copy
        copy.transform.localScale = new Vector3(1, 1, 1); // scale it
        copy.transform.parent = null;
        //remove scripts
        Destroy(copy.GetComponent<MouseClickHandler>());
        Destroy(copy.GetComponent<PutInBasket>());

        //add physics
        copy.AddComponent<Rigidbody>();
        copy.GetComponent<Rigidbody>().useGravity = true;


        //add new scripts
        copy.AddComponent<RemoveFromCounter>();
        copy.GetComponent<RemoveFromCounter>().SetCashRegister(cashregister);

        if (!cashregister.GetComponent<BasketHandler>().PutInBasket(copy)) // place the object at the right position and decide if it should be removed
        {
            Destroy(copy);
        }      
    }
}
