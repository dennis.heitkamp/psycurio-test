using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/**
 * Checks if an object is clicked by the user and then calls other functions. 
 * */
public class MouseClickHandler : MonoBehaviour
{
    [SerializeField]
    private UnityEvent otherFunction; //function to be called on click
   
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //shoot raycast

            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.transform != null)
                {
                    if (hit.transform.gameObject.name == gameObject.name) // if this object is hit by the raycast
                    {
                        otherFunction.Invoke(); //call other function
                    }                 
                }
            }
            
        }
    }



}
