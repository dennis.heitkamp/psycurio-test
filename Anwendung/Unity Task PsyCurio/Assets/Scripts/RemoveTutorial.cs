using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Controll the view of the camera. View the tutorial or the shop.
 * */
public class RemoveTutorial : MonoBehaviour
{
    [SerializeField]
    private GameObject tutorial;

    [SerializeField]
    private Camera cam;

    private Vector3 tutRotation = new Vector3(0f,180f,0f);
    private Vector3 tutPosition = new Vector3(10f, 10f, 10f);

    private Vector3 roomPosition = new Vector3(0.3f, 0.6f, -1.03f);
    private Vector3 roomRotation = new Vector3(6.2f, 0f, 0f);

    /*
     * View the shop.
     * */
    public void SwitchViewToRoom()
    {
        cam.transform.eulerAngles = roomRotation;
        cam.transform.position = roomPosition;
        tutorial.SetActive(false);
    }
    /*
     * View the tutorial. 
     * */
    public void SwitchViewToTutorial()
    {
        cam.transform.eulerAngles = tutRotation;
        cam.transform.position = tutPosition;
        tutorial.SetActive(true);
    }
}
