# PsyCurio test

The latest version of the task is on the main branch with the tag "submission" and it is in the scene "shop_improved".

I decided to write three unit test for the key functions of the tasks:
- Calculating the total price of the objects
- testing the maximum amount of items on the counter
- placing the objects at the right position

I added on additional feature:
- the speech bubble can be stoped by clicking the cash register again. On the next click on the casj register the interaction acts normally.

Version 2:
I improved the scene with the provided feedback:
- public variables are replaced by \[SerialField\] private ...
- the items are listed all at once
- items can be removed from the counter
- Iteminfo is deleted
- the "purchase" button is added
- hovering over objects outlines them

Also I added more usability to the scene:
- there is a tutorial that explains what is possible in the scene
- some signs tell what an object can do
- there is the possibility to go back to the tutorial and enable the help signs again



